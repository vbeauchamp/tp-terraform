data "aws_vpc" "default" {
  default = true
}

data "aws_subnet_ids" "subnets" {
  vpc_id = data.aws_vpc.default.id
}

resource "aws_security_group" "demo_sg" {
  name_prefix = "demo_sg"
  description = "Only for demo"
  vpc_id      = data.aws_vpc.default.id
  ingress {
    from_port = 80
    protocol  = "tcp"
    to_port   = 80
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port = 0
    to_port   = 0
    protocol  = "all"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "my_instances" {
  count           = length(data.aws_subnet_ids.subnets)
  ami             = "ami-0d73480446600f555"
  instance_type   = "t2.medium"
  subnet_id       = data.aws_subnet_ids.subnets[count.index]
  security_groups = [aws_security_group.demo_sg.id]
  user_data = templatefile("cloud-init.sh", {index = count.index})

  tags = {
    Name      = "instance-demo-${count.index}"
    Formation = "terraform"
    User      = "VABE"
  }
}

resource "aws_lb" "demo_lb" {
  name               = "demo_lb_formation_terraform"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.demo_sg.id]
  subnets            = data.aws_subnet_ids.subnets

  enable_deletion_protection = true

  tags = {
    Formation = "terraform"
    User      = "VABE"
  }
}

resource "aws_lb_target_group" "demo_lb_target_group" {
  name        = "tf-example-lb-tg"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = data.aws_vpc.default.id
  target_type = "instance"
}

resource "aws_lb_target_group_attachment" "instance_demo_attachement" {
  count            = length(aws_instance.my_instances)
  target_group_arn = aws_lb_target_group.demo_lb_target_group.arn
  target_id        = aws_instance.my_instances[count.index].id
}

resource "aws_lb_listener" "demo-front" {
  load_balancer_arn = aws_lb.demo_lb.arn
  port              = 80
  default_action {
    type = "forward"
  }
}

output "elb_address" {
  value = aws_lb.demo_lb.dns_name
}