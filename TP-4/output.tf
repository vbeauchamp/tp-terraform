output "jenkins_ip" {
  value = aws_instance.jenkins.private_ip
}

output "jenkins_slaves_ip" {
  value = aws_instance.jenkins-slave.*.private_ip
}