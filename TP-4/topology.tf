
resource "aws_subnet" "my_subnet" {
  availability_zone = local.availability_zone
  cidr_block        = var.subnet_cidr_block
  vpc_id            = data.aws_vpc.my_vpc.id
  tags = {
    Name      = "subnet-TF-${local.resource_name_suffix}"
    Formation = "terraform"
    User      = var.trigramme
  }
}

resource "aws_security_group" "my_security_group" {
  vpc_id      = data.aws_vpc.my_vpc.id
  name_prefix = "TF-${var.tp}_SG"

  tags = {
    Name      = "sg-TF-${local.resource_name_suffix}"
    Formation = var.Formation
    User      = var.trigramme
  }

  dynamic "ingress" {
    for_each = var.ingress_rules_configuration
    content {
      from_port   = ingress.value.port
      to_port     = ingress.value.port
      protocol    = ingress.value.protocol
      cidr_blocks = ingress.value.cidr_blocks # add a CIDR block here
    }
  }
}

resource "aws_instance" "jenkins" {
  ami                    = data.aws_ami.centos.id
  instance_type          = "t2.medium"
  subnet_id              = aws_subnet.my_subnet.id
  vpc_security_group_ids = [aws_security_group.my_security_group.id]

  tags = {
    Name      = "jenkins-TF-${local.resource_name_suffix}"
    Formation = "terraform"
    User      = var.trigramme
  }

  lifecycle {
    ignore_changes = [
      ami,
    ]
  }
}

resource "aws_instance" "jenkins-slave" {
  ami                    = data.aws_ami.centos.id
  instance_type          = "t2.micro"
  subnet_id              = aws_subnet.my_subnet.id
  vpc_security_group_ids = [aws_security_group.my_security_group.id]
  count                  = var.number_of_jenkins_slave
  tags = {
    Name      = "jenkins-slave-TF-${local.resource_name_suffix}"
    Formation = "terraform"
    User      = var.trigramme
  }

  depends_on = [aws_instance.jenkins]

  lifecycle {
    ignore_changes = [
      ami,
    ]
  }

}