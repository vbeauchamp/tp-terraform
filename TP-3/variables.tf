variable "trigramme" {
  type    = string
  default = "VABE"
}

variable "tp" {
  type    = string
  default = "TP3"
}

variable "subnet_cidr_block" {
  type    = string
  default = "10.0.4.0/24"
}

variable "availability_zone_suffix" {
  type    = string
  default = "c"
}

variable "formation" {
  type =string
  default = "terraform"
}
